package me.juanco.mvm.mobs;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityZombie;
import net.minecraft.server.v1_8_R3.MathHelper;
import net.minecraft.server.v1_8_R3.PathfinderGoalFloat;
import net.minecraft.server.v1_8_R3.PathfinderGoalHurtByTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_8_R3.PathfinderGoalMoveThroughVillage;
import net.minecraft.server.v1_8_R3.PathfinderGoalMoveTowardsRestriction;
import net.minecraft.server.v1_8_R3.PathfinderGoalNearestAttackableTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalRandomLookaround;
import net.minecraft.server.v1_8_R3.PathfinderGoalRandomStroll;

public class NormalZombie extends EntityZombie {

	private final int maxHealth;

	public NormalZombie(Location loc, int maxHealth, boolean minion) {
		super(((CraftWorld) loc.getWorld()).getHandle());

		if (minion)
			setBaby(true);

		NMSUtils.clearGoals(goalSelector);
		NMSUtils.clearGoals(targetSelector);

		defineGoals(minion);

		teleportTo(loc, false);

		setCustomName(ChatColor.translateAlternateColorCodes('&', "&cZombie-Bot"));
		setCustomNameVisible(true);

		datawatcher.watch(6, maxHealth);

		this.maxHealth = maxHealth;
	}

	@Override
	public void setHealth(float f) {
		datawatcher.watch(6, Float.valueOf(MathHelper.a(f, 0.0F, maxHealth)));
	}

	private void defineGoals(boolean minion) {
		goalSelector.a(1, new PathfinderGoalFloat(this));
		if (!minion) {
			goalSelector.a(2, new PathfinderGoalMeleeAttack(this, NexusSilverfish.class, 1.0D, true));
			goalSelector.a(3, new PathfinderGoalMeleeAttack(this, EntityHuman.class, 1.0D, false));
			goalSelector.a(4, new PathfinderGoalMoveTowardsRestriction(this, 1.0D));
			goalSelector.a(5, new PathfinderGoalMoveThroughVillage(this, 1.0D, false));
		} else {
			goalSelector.a(2, new PathfinderGoalMeleeAttack(this, EntityHuman.class, 0.5D, false));
			goalSelector.a(4, new PathfinderGoalMoveTowardsRestriction(this, 2.0D));
			goalSelector.a(5, new PathfinderGoalMoveThroughVillage(this, 2.0D, false));
		}

		goalSelector.a(6, new PathfinderGoalRandomStroll(this, 1.0D));
		if (!minion)
			goalSelector.a(7, new PathfinderGoalLookAtPlayer(this, NexusSilverfish.class, 80.0F));
		goalSelector.a(8, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 10.0F));
		goalSelector.a(8, new PathfinderGoalRandomLookaround(this));
		targetSelector.a(1, new PathfinderGoalHurtByTarget(this, true));
		targetSelector.a(2, new PathfinderGoalNearestAttackableTarget<NexusSilverfish>(this, NexusSilverfish.class, 80,
				true, false, null));
	}
}
