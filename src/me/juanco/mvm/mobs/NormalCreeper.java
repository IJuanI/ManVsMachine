package me.juanco.mvm.mobs;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import net.minecraft.server.v1_8_R3.EntityCreeper;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.MathHelper;
import net.minecraft.server.v1_8_R3.PathfinderGoalFloat;
import net.minecraft.server.v1_8_R3.PathfinderGoalHurtByTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_8_R3.PathfinderGoalNearestAttackableTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalRandomLookaround;
import net.minecraft.server.v1_8_R3.PathfinderGoalRandomStroll;
import net.minecraft.server.v1_8_R3.PathfinderGoalSwell;

public class NormalCreeper extends EntityCreeper {

	private final int maxHealth;

	public NormalCreeper(Location loc, int maxHealth) {
		super(((CraftWorld) loc.getWorld()).getHandle());

		NMSUtils.clearGoals(goalSelector);
		NMSUtils.clearGoals(targetSelector);

		defineGoals();

		teleportTo(loc, false);

		setCustomName(ChatColor.translateAlternateColorCodes('&', "&cBomb-o-tron"));
		setCustomNameVisible(true);

		datawatcher.watch(6, maxHealth);

		this.maxHealth = maxHealth;
	}

	@Override
	public void setHealth(float f) {
		datawatcher.watch(6, Float.valueOf(MathHelper.a(f, 0.0F, maxHealth)));
	}

	private void defineGoals() {
		goalSelector.a(1, new PathfinderGoalFloat(this));
		goalSelector.a(2, new PathfinderGoalSwell(this));
		goalSelector.a(3, new PathfinderGoalMeleeAttack(this, NexusSilverfish.class, 1.0D, true));
		goalSelector.a(4, new PathfinderGoalMeleeAttack(this, EntityHuman.class, 1.0D, false));
		goalSelector.a(5, new PathfinderGoalRandomStroll(this, 0.8D));
		goalSelector.a(6, new PathfinderGoalLookAtPlayer(this, NexusSilverfish.class, 80.0F));
		goalSelector.a(7, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 10.0F));
		goalSelector.a(7, new PathfinderGoalRandomLookaround(this));
		targetSelector.a(1, new PathfinderGoalNearestAttackableTarget<NexusSilverfish>(this, NexusSilverfish.class, 80,
				true, false, null));
		targetSelector.a(2, new PathfinderGoalHurtByTarget(this, true));
	}

}
