package me.juanco.mvm.mobs;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntitySkeleton;
import net.minecraft.server.v1_8_R3.MathHelper;
import net.minecraft.server.v1_8_R3.PathfinderGoalFloat;
import net.minecraft.server.v1_8_R3.PathfinderGoalHurtByTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_8_R3.PathfinderGoalMoveThroughVillage;
import net.minecraft.server.v1_8_R3.PathfinderGoalMoveTowardsRestriction;
import net.minecraft.server.v1_8_R3.PathfinderGoalRandomLookaround;
import net.minecraft.server.v1_8_R3.PathfinderGoalRandomStroll;

public class WitherSkeletonBoss extends EntitySkeleton {

	private final int maxHealth;

	public WitherSkeletonBoss(Location loc, int maxHealth) {
		super(((CraftWorld) loc.getWorld()).getHandle());

		NMSUtils.clearGoals(goalSelector);
		NMSUtils.clearGoals(targetSelector);

		defineGoals();
		setSkeletonType(1);
		setSize(1.44F, 5.07F);

		teleportTo(loc, false);

		setCustomName(ChatColor.translateAlternateColorCodes('&', "&6&lThe Matrix"));
		setCustomNameVisible(true);

		datawatcher.watch(6, maxHealth);

		this.maxHealth = maxHealth;
	}

	@Override
	public void setHealth(float f) {
		datawatcher.watch(6, Float.valueOf(MathHelper.a(f, 0.0F, maxHealth)));
	}

	private void defineGoals() {
		goalSelector.a(0, new PathfinderGoalFloat(this));
		goalSelector.a(2, new PathfinderGoalMeleeAttack(this, EntityHuman.class, 10.0D, false));
		goalSelector.a(5, new PathfinderGoalMoveTowardsRestriction(this, 0.3D));
		goalSelector.a(6, new PathfinderGoalMoveThroughVillage(this, 0.3D, false));
		goalSelector.a(7, new PathfinderGoalRandomStroll(this, 0.3D));
		goalSelector.a(8, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 20.0F));
		goalSelector.a(8, new PathfinderGoalRandomLookaround(this));
		targetSelector.a(1, new PathfinderGoalHurtByTarget(this, true));
	}

}
