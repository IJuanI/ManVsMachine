package me.juanco.mvm.mobs;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import net.minecraft.server.v1_8_R3.DamageSource;
import net.minecraft.server.v1_8_R3.EntitySilverfish;

public class NexusSilverfish extends EntitySilverfish {

	public NexusSilverfish(Location loc) {
		super(((CraftWorld) loc.getWorld()).getHandle());

		NMSUtils.clearGoals(goalSelector);
		NMSUtils.clearGoals(targetSelector);

		teleportTo(loc, false);

		setCustomName(ChatColor.translateAlternateColorCodes('&', "&4&lNEXUS"));
		setCustomNameVisible(true);
	}

	@Override
	public boolean damageEntity(DamageSource damagesource, float f) {
		return false;
	}

	@Override
	public boolean isInvulnerable(DamageSource damagesource) {
		return true;
	}

	@Override
	public boolean isInvisible() {
		return true;
	}

	@Override
	public void die() {
	}

	@Override
	public void die(DamageSource damagesource) {
	}

	public void delete() {
		super.die();
	}

	@Override
	public void makeSound(String s, float f, float f1) {
	}
}
