package me.juanco.mvm.mobs;

import net.minecraft.server.v1_8_R3.Entity;

public enum MechTypes {

	Nexus("Silverfish", 60, NexusSilverfish.class),
	CustomZombie("Zombie", 54, NormalZombie.class),
	NormalCreeper("Creeper", 50, NormalCreeper.class),
	ZombieBoss("Giant", 53, ZombieBoss.class), WitherSkeletonBoss("Skeleton", 51, WitherSkeletonBoss.class);
	
	private MechTypes(String name, int id, Class<? extends Entity> custom) {
	}
}
