package me.juanco.mvm.mobs;

import java.lang.reflect.Field;
import java.util.List;

import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;

public class NMSUtils {

	private static Object getPrivateField(String fieldName, Class<?> clazz, Object object) {
		Field field;
		Object o = null;

		try {
			field = clazz.getDeclaredField(fieldName);

			field.setAccessible(true);

			o = field.get(object);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return o;
	}

	public static void clearGoals(Object selector) {
		((List<?>) NMSUtils.getPrivateField("b", PathfinderGoalSelector.class, selector)).clear();
		((List<?>) NMSUtils.getPrivateField("c", PathfinderGoalSelector.class, selector)).clear();

	}
}
