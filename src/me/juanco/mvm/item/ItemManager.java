package me.juanco.mvm.item;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemManager {

	private ItemUtils utils;

	public ItemStack splitter = utils.getItemStack(Material.STAINED_GLASS_PANE, 2, "&f");

	public ItemStack kitSelector = utils.getItemStack(Material.NETHER_STAR, "&6&lElije un kit");

	public ItemStack wrench = utils.getItemStack(Material.GOLD_HOE, "&c&lLlave Inglesa");

	public ItemManager() {
		utils = new ItemUtils();
	}

	public ItemUtils getUtils() {
		return utils;
	}
}
