package me.juanco.mvm.item;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class ItemUtils {

	public ItemStack getItemStack(Material mat, String name) {
		ItemStack item = new ItemStack(mat);

		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));

		item.setItemMeta(meta);

		return item;
	}

	public ItemStack getItemStack(Material mat, int data, String name) {
		ItemStack item = new ItemStack(mat, 1, (short) data);

		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));

		item.setItemMeta(meta);

		return item;
	}

	public ItemStack getItemStack(Material mat, int data, String name, Enchantment enchant, int level) {
		ItemStack item = new ItemStack(mat, 1, (short) data);

		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));

		meta.addEnchant(enchant, level, true);

		item.setItemMeta(meta);

		return item;
	}

	public ItemStack getItemStack(Material mat, int data, String name, Enchantment[] enchants, int[] levels) {
		ItemStack item = new ItemStack(mat, 1, (short) data);

		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));

		for (int index = 0; index < enchants.length; index++)
			meta.addEnchant(enchants[index], levels[index], true);

		item.setItemMeta(meta);

		return item;
	}
}
