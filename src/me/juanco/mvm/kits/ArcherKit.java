package me.juanco.mvm.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.juanco.mvm.game.GameManager;

public class ArcherKit extends Kits {

	private GameManager manager;

	public ArcherKit(GameManager manager) {
		this.manager = manager;
	}

	@Override
	public ItemStack getLogo() {
		return getItem(manager, Material.BOW, "&f&lKit Arquero");
	}

	@Override
	public ItemStack[] getArmorContents() {
		return new ItemStack[] {
				getItem(manager, Material.LEATHER_HELMET, "&f&lKit Arquero", Enchantment.PROTECTION_PROJECTILE, 3),
				getItem(manager, Material.LEATHER_CHESTPLATE, "&f&lKit Arquero"),
				getItem(manager, Material.LEATHER_LEGGINGS, "&f&lKit Arquero"),
				getItem(manager, Material.LEATHER_BOOTS, "&f&lKit Arquero") };
	}

	@Override
	public ItemStack[] getItems() {
		return new ItemStack[] {
				getItem(manager, Material.BOW, "&f&lArco de Snipper",
						new Enchantment[] { Enchantment.ARROW_DAMAGE, Enchantment.ARROW_INFINITE }, new int[] { 3, 1 }),
				getItem(manager, Material.WOOD_SWORD, "&f&lDaga Corta", Enchantment.FIRE_ASPECT, 1),
				new ItemStack(Material.POTION, 5, (short) 16421), new ItemStack(Material.ARROW) };
	}

	@Override
	public PotionEffect[] getEffects() {
		return new PotionEffect[] {
				new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.WATER_BREATHING, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.BLINDNESS, 5, 1),
				new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 15, 4)
		};
	}

	@Override
	public int getMoney() {
		return 400;
	}

	@Override
	public int getID() {
		return 2;
	}

	@Override
	public String getName() {
		return "Arquero";
	}

	@Override
	public String getPrefix() {
		return "&f&lA &r";
	}

	@Override
	public String getDisplay() {
		return "&f&lArquero &r";
	}

}
