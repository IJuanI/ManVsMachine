package me.juanco.mvm.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.juanco.mvm.game.GameManager;

public class KnightKit extends Kits {

	private GameManager manager;

	public KnightKit(GameManager manager) {
		this.manager = manager;
	}

	@Override
	public ItemStack getLogo() {
		return getItem(manager, Material.IRON_SWORD, "&2&lKit Caballero");
	}

	@Override
	public ItemStack[] getArmorContents() {
		return new ItemStack[] {
				getItem(manager, Material.LEATHER_HELMET, "&2&lKit Caballero", Enchantment.PROTECTION_ENVIRONMENTAL, 1),
				getItem(manager, Material.CHAINMAIL_CHESTPLATE, "&2&lKit Caballero",
						Enchantment.PROTECTION_ENVIRONMENTAL, 2),
				getItem(manager, Material.LEATHER_LEGGINGS, "&2&lKit Caballero", Enchantment.PROTECTION_ENVIRONMENTAL,
						2),
				getItem(manager, Material.LEATHER_BOOTS, "&2&lKit Caballero", Enchantment.PROTECTION_ENVIRONMENTAL,
						1) };
	}

	@Override
	public ItemStack[] getItems() {
		return new ItemStack[] {
				getItem(manager, Material.IRON_SWORD, "&2&lExcalibur", new Enchantment[] { Enchantment.DAMAGE_ALL },
						new int[] { 3 }),
				new ItemStack(Material.POTION, 2, (short) 16421), new ItemStack(Material.POTION, 1, (short) 16418) };
	}

	@Override
	public PotionEffect[] getEffects() {
		return new PotionEffect[] { new PotionEffect(PotionEffectType.ABSORPTION, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.BLINDNESS, 5, 1),
				new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 15, 4),
				new PotionEffect(PotionEffectType.REGENERATION, 20, 1) };
	}

	@Override
	public int getMoney() {
		return 350;
	}

	@Override
	public int getID() {
		return 1;
	}

	@Override
	public String getName() {
		return "Caballero";
	}

	@Override
	public String getPrefix() {
		return "&2&lC &r";
	}

	@Override
	public String getDisplay() {
		return "&2&lCaballero &r";
	}

}
