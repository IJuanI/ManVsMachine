package me.juanco.mvm.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.juanco.mvm.game.GameManager;

public class EngineerKit extends Kits {

	private GameManager manager;

	public EngineerKit(GameManager manager) {
		this.manager = manager;
	}

	@Override
	public ItemStack getLogo() {
		return getItem(manager, Material.REDSTONE_WIRE, "&c&lKit Ingeniero");
	}

	@Override
	public ItemStack[] getArmorContents() {
		return new ItemStack[] {
				getItem(manager, Material.CHAINMAIL_HELMET, "&c&lKit Ingeniero", Enchantment.PROTECTION_PROJECTILE, 2),
				getItem(manager, Material.LEATHER_CHESTPLATE, "&c&lKit Ingeniero", Enchantment.PROTECTION_ENVIRONMENTAL,
						1),
				getItem(manager, Material.LEATHER_LEGGINGS, "&c&lKit Ingeniero", Enchantment.PROTECTION_ENVIRONMENTAL,
						1),
				getItem(manager, Material.LEATHER_BOOTS, "&c&lKit Ingeniero", Enchantment.PROTECTION_FALL, 2) };
	}

	@Override
	public ItemStack[] getItems() {
		return new ItemStack[] {
				getItem(manager, Material.STONE_SWORD, "&c&lEspada Inteligente",
						new Enchantment[] { Enchantment.DAMAGE_ALL, Enchantment.FIRE_ASPECT, Enchantment.KNOCKBACK },
						new int[] { 2, 1, 1 }),
				manager.getItemManager().wrench, new ItemStack(Material.POTION, 3, (short) 16421),
				new ItemStack(Material.POTION, 2, (short) 16418) };
	}

	@Override
	public PotionEffect[] getEffects() {
		return new PotionEffect[] {
				new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1),
				new PotionEffect(PotionEffectType.WEAKNESS, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.SATURATION, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.BLINDNESS, 5, 1),
				new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 15, 4),
				new PotionEffect(PotionEffectType.REGENERATION, 20, 1)
		};
	}

	@Override
	public int getMoney() {
		return 200;
	}

	@Override
	public int getID() {
		return 3;
	}

	@Override
	public String getName() {
		return "Ingeniero";
	}

	@Override
	public String getPrefix() {
		return "&c&lI &r";
	}

	@Override
	public String getDisplay() {
		return "&c&lIngeniero &r";
	}

}
