package me.juanco.mvm.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.juanco.mvm.game.GameManager;

public class SonicKit extends Kits {

	private GameManager manager;

	public SonicKit(GameManager manager) {
		this.manager = manager;
	}

	@Override
	public ItemStack getLogo() {
		return getItem(manager, Material.FEATHER, "&e&lKit Sonic");
	}

	@Override
	public ItemStack[] getArmorContents() {
		return new ItemStack[] {
				getItem(manager, Material.LEATHER_HELMET, "&e&lKit Sonic"),
				getItem(manager, Material.LEATHER_CHESTPLATE, "&e&lKit Sonic", Enchantment.PROTECTION_ENVIRONMENTAL, 2),
				getItem(manager, Material.LEATHER_LEGGINGS, "&e&lKit Sonic"),
				getItem(manager, Material.LEATHER_BOOTS, "&e&lKit Sonic") };
	}

	@Override
	public ItemStack[] getItems() {
		return new ItemStack[] {
				getItem(manager, Material.WOOD_SWORD, "&e&lEspada ligera",
						new Enchantment[] { Enchantment.DAMAGE_ALL, Enchantment.KNOCKBACK, Enchantment.FIRE_ASPECT },
						new int[] { 1, 1, 1 }),
				new ItemStack(Material.POTION, 6, (short) 16421) };
	}

	@Override
	public PotionEffect[] getEffects() {
		return new PotionEffect[] { new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1),
				new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.BLINDNESS, 5, 1),
				new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 15, 4) };
	}

	@Override
	public int getMoney() {
		return 400;
	}

	@Override
	public int getID() {
		return 4;
	}

	@Override
	public String getName() {
		return "Sonic";
	}

	@Override
	public String getPrefix() {
		return "&e&lS &r";
	}

	@Override
	public String getDisplay() {
		return "&e&lSonic &r";
	}

}
