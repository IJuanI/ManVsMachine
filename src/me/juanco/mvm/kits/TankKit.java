package me.juanco.mvm.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.juanco.mvm.game.GameManager;

public class TankKit extends Kits {

	private GameManager manager;

	public TankKit(GameManager manager) {
		this.manager = manager;
	}

	@Override
	public ItemStack getLogo() {
		return getItem(manager, Material.IRON_CHESTPLATE, "&b&lKit Tanque");
	}

	@Override
	public ItemStack[] getArmorContents() {
		return new ItemStack[] {
				getItem(manager, Material.DIAMOND_HELMET, "&b&lKit Tanque"),
				getItem(manager, Material.IRON_CHESTPLATE, "&b&lKit Tanque",
						new Enchantment[] { Enchantment.PROTECTION_ENVIRONMENTAL, Enchantment.THORNS },
						new int[] { 3, 1 }),
				getItem(manager, Material.IRON_LEGGINGS, "&b&lKit Tanque"),
				getItem(manager, Material.IRON_BOOTS, "&b&lKit Tanque") };
	}

	@Override
	public ItemStack[] getItems() {
		return new ItemStack[] {
				getItem(manager, Material.STONE_SWORD, "&b&lEspada Tanque",
						new Enchantment[] { Enchantment.DAMAGE_ALL, Enchantment.KNOCKBACK }, new int[] { 2, 1 }),
				new ItemStack(Material.POTION, 3, (short) 16421) };
	}

	@Override
	public PotionEffect[] getEffects() {
		return new PotionEffect[] { new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 0),
				new PotionEffect(PotionEffectType.BLINDNESS, 5, 1),
				new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 15, 4) };
	}

	@Override
	public int getMoney() {
		return 450;
	}

	@Override
	public int getID() {
		return 0;
	}

	@Override
	public String getName() {
		return "Tanque";
	}

	@Override
	public String getPrefix() {
		return "&b&lT &r";
	}

	@Override
	public String getDisplay() {
		return "&b&lTanque &r";
	}

}
