package me.juanco.mvm.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import me.juanco.mvm.game.GameManager;

public abstract class Kits {

	public abstract ItemStack getLogo();

	public abstract ItemStack[] getArmorContents();

	public abstract ItemStack[] getItems();

	public abstract PotionEffect[] getEffects();

	public abstract int getMoney();

	public abstract int getID();

	public abstract String getName();

	public abstract String getPrefix();

	public abstract String getDisplay();

	protected ItemStack getItem(GameManager manager, Material mat, String name) {
		return manager.getItemManager().getUtils().getItemStack(mat, name);
	}

	protected ItemStack getItem(GameManager manager, Material mat, int data, String name) {
		return manager.getItemManager().getUtils().getItemStack(mat, data, name);
	}

	protected ItemStack getItem(GameManager manager, Material mat, String name, Enchantment enchant, int level) {
		return manager.getItemManager().getUtils().getItemStack(mat, 0, name, enchant, level);
	}

	protected ItemStack getItem(GameManager manager, Material mat, String name, Enchantment[] enchants, int[] levels) {
		return manager.getItemManager().getUtils().getItemStack(mat, 0, name, enchants, levels);
	}
}
