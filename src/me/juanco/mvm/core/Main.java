package me.juanco.mvm.core;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import me.juanco.mvm.game.GameManager;
import me.juanco.mvm.listeners.AsyncPlayerChatListener;
import me.juanco.mvm.listeners.DamageListener;
import me.juanco.mvm.listeners.InventoryListener;
import me.juanco.mvm.listeners.ManListener;

public class Main extends JavaPlugin {

	private GameManager manager;

	private final static String prefix = "&7[&b&l!&7] &e";

	@Override
	public void onEnable() {
		ManListener.setup(this);

		manager = new GameManager();

		new InventoryListener(manager);
		new AsyncPlayerChatListener(manager);
		new DamageListener(manager);

		new Timer(this);
	}

	public GameManager getManager() {
		return manager;
	}

	public static void broadcast(String message) {
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', prefix + message));
	}
}
