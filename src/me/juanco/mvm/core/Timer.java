package me.juanco.mvm.core;

import org.bukkit.scheduler.BukkitRunnable;

public class Timer extends BukkitRunnable {

	private final Main core;

	public Timer(Main core) {
		runTaskTimer(core, 10, 20);
		this.core = core;
	}

	@Override
	public void run() {
		core.getManager().tickSecond();
	}
}
