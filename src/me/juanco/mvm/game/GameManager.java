package me.juanco.mvm.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import me.juanco.mvm.core.Main;
import me.juanco.mvm.data.DataManager;
import me.juanco.mvm.gui.KitsGUI;
import me.juanco.mvm.item.ItemManager;
import me.juanco.mvm.kits.ArcherKit;
import me.juanco.mvm.kits.EngineerKit;
import me.juanco.mvm.kits.Kits;
import me.juanco.mvm.kits.KnightKit;
import me.juanco.mvm.kits.SonicKit;
import me.juanco.mvm.kits.TankKit;

public class GameManager {

	private List<Player> players = new ArrayList<Player>();
	private HashMap<Player, ManPlayer> playerData = new HashMap<Player, ManPlayer>();

	private List<Player> respawnQueue = new ArrayList<Player>();
	private List<Player> alive = new ArrayList<Player>();

	private int time = 0;
	private GameStatus status = GameStatus.WAITING;
	private DataManager data = new DataManager();
	private ItemManager item = new ItemManager();
	private KitsGUI kitsGui;

	private List<Kits> kits = new ArrayList<Kits>();

	public GameManager() {
		kits.add(new KnightKit(this));
		kits.add(new TankKit(this));
		kits.add(new EngineerKit(this));
		kits.add(new ArcherKit(this));
		kits.add(new SonicKit(this));

		kitsGui = new KitsGUI(this);
	}

	public void playerJoin(Player player) {
		players.add(player);
		if (status.ordinal() < 2)
			setupWaiter(player);
		else
			setupSpec(player);

		playerData.put(player, new ManPlayer(player, kits.get(0)));
	}

	public void playerQuit(Player player) {
		if (players.contains(player)) {
			players.remove(player);
			playerData.remove(player);

			if (respawnQueue.contains(player))
				respawnQueue.remove(player);
			else if (alive.contains(player))
				alive.remove(player);
		}
	}

	public void teleportPlayer(Player player) {
		if (status == GameStatus.WAITING)
			player.teleport(data.waitLoc);
		else if (status == GameStatus.STARTING)
			player.teleport(data.gameLoc);
		else
			player.teleport(data.specLoc);
	}

	public void setupSpec(Player player) {
		teleportPlayer(player);

		resetInventory(player);
		player.setGameMode(GameMode.SPECTATOR);

		for (Player p : players)
			if (!p.getGameMode().equals(GameMode.SPECTATOR))
				p.hidePlayer(player);
			else if (!player.canSee(p))
				player.showPlayer(p);
	}

	public void init() {
		time = 30;
		status = GameStatus.STARTING;
	}

	private void resetInventory(Player player) {
		player.getInventory().clear();
		player.getInventory().setArmorContents(new ItemStack[4]);
	}

	public void setupWaiter(Player player) {
		teleportPlayer(player);

		resetInventory(player);
		player.setGameMode(GameMode.ADVENTURE);

		player.getInventory().addItem(getItemManager().kitSelector);
	}

	public ItemManager getItemManager() {
		return item;
	}

	public List<Kits> getKits() {
		return kits;
	}

	public KitsGUI getkitsGui() {
		return kitsGui;
	}

	public boolean hasData(Player player) {
		return playerData.containsKey(player);
	}

	public ManPlayer getData(Player player) {
		return playerData.get(player);
	}

	public int getTime() {
		return time;
	}

	public int getStatus() {
		return status.ordinal();
	}

	public void setKit(Player player, Kits kit) {
		ManPlayer data = playerData.get(player);
		if (data.kit != kit) {
			data.kit = kit;
			updateScoreboard(player);
			
			if (status.ordinal() > 1)
				Main.broadcast(player.getDisplayName() + " &ecambio al kit " + kit.getDisplay());
		}
	}

	public void updateScoreboard(Player player) {

	}

	public void giveKit(Player player, Kits kit) {
		for (PotionEffect effect : player.getActivePotionEffects())
			player.removePotionEffect(effect.getType());

		for (PotionEffect effect : kit.getEffects())
			player.addPotionEffect(effect);

		player.getInventory().setArmorContents(kit.getArmorContents());
		player.getInventory().setContents(kit.getItems());
	}

	public void tickSecond() {
		if (time > -1) {
			time--;
			for (Player player : players)
				updateScoreboard(player);
			if (time == 0 && status.ordinal() == 1)
				start();
		}

		if (status.ordinal() == 2)
			flushRespawnQueue();

		for (Player player : alive)
			playerData.get(player).time++;
	}

	private void flushRespawnQueue() {
		for (Player player : respawnQueue) {
			int time = playerData.get(player).time--;
			if (time < 1)
				respawn(player);
		}
	}

	public void start() {
		Main.broadcast("Que empiece la masacre! 3:D");

		for (Player player : players)
			spawn(player);
	}

	public void die(Player player) {
		alive.remove(player);
		player.setGameMode(GameMode.SPECTATOR);
		player.setHealth(player.getMaxHealth());
		player.teleport(data.specLoc);

		for (Player p : players)
			if (!p.getGameMode().equals(GameMode.SPECTATOR))
				p.hidePlayer(player);
			else if (!player.canSee(p))
				player.showPlayer(p);

		ManPlayer data = playerData.get(player);

		if (data.time < 30)
			data.time = 3;
		else if (data.time < 60)
			data.time = 5;
		else if (data.time < 120)
			data.time = 10;
		else if (data.time < 300)
			data.time = 15;
		else if (data.time < 480)
			data.time = 20;
		else
			data.time = 30;

		Main.broadcast(player.getCustomName() + " &cha caido");
	}

	public void spawn(Player player) {
		ManPlayer data = playerData.get(player);
		data.time = 0;
		giveKit(player, data.kit);
		player.teleport(this.data.gameLoc);

		for (Player p : players)
			p.showPlayer(player);

		alive.add(player);
	}

	public void respawn(Player player) {
		spawn(player);
		Main.broadcast(player.getCustomName() + " &aha respawneado");
	}
}
