package me.juanco.mvm.game;

import org.bukkit.entity.Player;

import me.juanco.mvm.kits.Kits;

public class ManPlayer {

	public final Player player;
	public Kits kit;

	public int time = 0;

	public ManPlayer(Player player, Kits kit) {
		this.player = player;
		this.kit = kit;
	}
}
