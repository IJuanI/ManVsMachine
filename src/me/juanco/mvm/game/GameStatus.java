package me.juanco.mvm.game;

public enum GameStatus {

	WAITING, STARTING, PLAYING, RESTARTING;
}
