package me.juanco.mvm.gui;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import me.juanco.mvm.game.GameManager;
import me.juanco.mvm.kits.Kits;

public class KitsGUI {

	private final Inventory inv;
	private final List<Kits> kits;

	public KitsGUI(GameManager manager) {
		Inventory inv = Bukkit.createInventory(null, 27,
				ChatColor.translateAlternateColorCodes('&', "&f&lElije un kit"));

		int index = 0;
		while (index < 10) {
			inv.setItem(index, manager.getItemManager().splitter);
			index++;
		}
		
		for (Kits kit : manager.getKits()) {
			if (index == 12 || index == 14) {
				inv.setItem(index, manager.getItemManager().splitter);
				index++;
			}
			
			inv.setItem(index, kit.getLogo());
			index++;
		}

		while (index < inv.getSize()) {
			inv.setItem(index, manager.getItemManager().splitter);
			index++;
		}
		
		this.inv = inv;
		kits = manager.getKits();
	}

	public void display(Player player) {
		player.openInventory(inv);
	}

	public Kits getKit(InventoryClickEvent e) {
		if (e.getClickedInventory().equals(inv))
			switch (e.getSlot()) {
			case 10:
				return kits.get(0);
			case 11:
				return kits.get(1);
			case 13:
				return kits.get(2);
			case 15:
				return kits.get(3);
			case 16:
				return kits.get(4);
			default:
				return null;
			}
		return null;
	}
}
