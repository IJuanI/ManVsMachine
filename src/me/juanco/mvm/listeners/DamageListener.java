package me.juanco.mvm.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;

import me.juanco.mvm.game.GameManager;

public class DamageListener extends ManListener {

	private final GameManager manager;

	public DamageListener(GameManager manager) {
		this.manager = manager;
	}

	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if (manager.getStatus() != 2)
			e.setCancelled(true);
		else if (e.getEntity() instanceof Player)
			if (e.getFinalDamage() >= ((Player) e.getEntity()).getHealth()) {
				manager.die((Player) e.getEntity());
				e.setCancelled(true);
			}
	}
}
