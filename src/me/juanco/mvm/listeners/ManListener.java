package me.juanco.mvm.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import me.juanco.mvm.core.Main;

public class ManListener implements Listener {

	private static Main core;

	public ManListener() {
		Bukkit.getPluginManager().registerEvents(this, core);
	}

	public static void setup(Main main) {
		core = main;
	}
}
