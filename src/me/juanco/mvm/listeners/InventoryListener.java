package me.juanco.mvm.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;

import me.juanco.mvm.game.GameManager;
import me.juanco.mvm.kits.Kits;

public class InventoryListener extends ManListener {

	private final GameManager manager;

	public InventoryListener(GameManager manager) {
		this.manager = manager;
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Kits kit = manager.getkitsGui().getKit(event);

		if (kit != null) {
			manager.setKit((Player) event.getWhoClicked(), kit);
			event.getWhoClicked().closeInventory();
		}

		event.setCancelled(true);
	}
}
