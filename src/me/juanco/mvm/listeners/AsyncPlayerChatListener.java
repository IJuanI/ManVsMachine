package me.juanco.mvm.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.juanco.mvm.game.GameManager;

public class AsyncPlayerChatListener extends ManListener {

	private final GameManager manager;

	public AsyncPlayerChatListener(GameManager manager) {
		this.manager = manager;
	}

	@EventHandler
	public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if (manager.hasData(player))
			event.setFormat(manager.getData(player).kit.getPrefix() + "%s: %s");
	}

}
